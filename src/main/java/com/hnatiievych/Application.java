package com.hnatiievych;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private final static Logger LOGGER = LogManager.getLogger(Application.class);
    public static void main(String[] args) {

        LOGGER.debug("This is a trace message");
        LOGGER.info("This is an info message");
        LOGGER.warn("This is a warn message");
        LOGGER.error("This is an error message");
        LOGGER.fatal("This is a fatal message");

    }
}

